#!/bin/bash
# This script displays the current pressure of a graphics tablet pen in a little dialog.
# Copy the name (not the id!) of the input device you want to capture from the command
# $ xinput list
# and paste it in between the quotation marks in the line below
MyDevice='HUION PenTablet Pen stylus'

# If necessary (the percentage value doesn't change), replace the '4' in '$4' 
# in the code line below with the correct column number of the output of 
# 'xinput test' (the one that changes with pressure change. Count the columns from 1.)
# Also replace the maximum value (here: 65536) with your maximum pressure value 
# that you can see displayed in 'xinput test' when you press hard enough with the pen.

stdbuf -o0 xinput test "$MyDevice" | stdbuf -o0 awk '{print int(substr($4,6)/65536*100)}' | while read x
do
  echo "$x"
done | zenity --progress \
  --title="Pen Pressure" \
  --text="Current pen pressure" \
  --percentage=0

if [ "$?" = -1 ] ; then
        zenity --error \
          --text="No more pressure. Ooof."
        exit
fi
