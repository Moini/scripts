#!/bin/bash

# Set the system to run this script when you plug in a graphics tablet.
# The numbers for the area need to be adjusted, also the names of the tablet itself.

logger "Tablet detected, configuring..."
sleep 2
export DISPLAY=":0"

stylusID=$(xsetwacom --list devices | grep stylus | head -1 | sed 's/.*id: //' | sed 's/[ \t].*//')


# xsetwacom --set "HUION PenTablet Pen stylus" area 0 0 31250 25000
xsetwacom --set $stylusID area 0 0 31250 25000
xsetwacom --set "HUION PenTablet Pad pad" Button 1 "key +ctrl s -ctrl"
xsetwacom --set "HUION PenTablet Pad pad" Button 2 "key shift"
xsetwacom --set "HUION PenTablet Pad pad" Button 3 "key alt"
xsetwacom --set "HUION PenTablet Pad pad" Button 8 "key KP_Add"
xsetwacom --set "HUION PenTablet Pad pad" Button 9 "key KP_Subtract"
xsetwacom --set "HUION PenTablet Pad pad" Button 10 "key +ctrl v -ctrl"
xsetwacom --set "HUION PenTablet Pad pad" Button 11 "key +ctrl x -ctrl"
xsetwacom --set "HUION PenTablet Pad pad" Button 12 "key ctrl"
logger "Huion Tablet keys and area configured."